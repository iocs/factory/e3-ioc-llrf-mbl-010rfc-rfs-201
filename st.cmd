require essioc

require sis8300llrf
require llrfsystem

epicsEnvSet("LLRF_P", "MBL-010RFC")
epicsEnvSet("IDX", "201")
epicsEnvSet("LLRF_R", "RFS-LLRF-$(IDX)")
epicsEnvSet("LLRF_DIG_R_1", "RFS-DIG-$(IDX)")
epicsEnvSet("LLRF_RFM_R_1", "RFS-RFM-$(IDX)")
epicsEnvSet("TSELPV", "$(LLRF_P):RFS-EVR-$(IDX):EvtACnt-I.TIME")
epicsEnvSet("RKLY", "RFS-Kly-210:")
epicsEnvSet("RCAV", "EMR-CAV-020")
epicsEnvSet("SEC", "mbl")
epicsEnvSet("RFSTID", "36")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(llrfsystem_DIR)/llrfsystem.iocsh")

iocInit
